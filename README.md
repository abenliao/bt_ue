# Simple Behavior Tree Sample

This project is a simple behavior tree sample implementation by following the tutorial in [Unreal Engine official documents](https://docs.unrealengine.com/4.26/en-US/InteractiveExperiences/ArtificialIntelligence/BehaviorTrees/BehaviorTreeQuickStart/) step by step. 

Please take in mind that some variables are not named as exact as those in the documents.

EQS is not included in the sample.

It is written with Unreal Engine 4.26.